Media
=====

Collection of media I have presented at various forums.

* ["Community is more important than code"](community_is_more_important_than_code.pdf) -- Keynote at Kharagpur Open Source Summit, Kshitij 2018, Kharagpur.

    _Apparently I had some influence over the organizers of the summit so I
    managed to convince them to invite me to give a keynote!_

* ["Demystifying FOSS Licenses"](foss_licenses_talk_py_delhi.pdf) -- PyDelhi
    Meetup, 13th January 2018.

* [“Tech is WEIRD”](https://www.youtube.com/watch?v=sv9S-25XKe4), lightning talk at SciPy 2016, Austin, USA

* [“What’s new with sympy solvers”](https://youtu.be/YCxQI4C34j8?t=7m55s) lightning talk at SciPy 2015 Austin USA

